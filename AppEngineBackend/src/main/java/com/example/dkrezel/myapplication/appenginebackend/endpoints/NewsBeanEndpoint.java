package com.example.dkrezel.myapplication.appenginebackend.endpoints;

import com.example.dkrezel.myapplication.appenginebackend.beans.NewsBean;
import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.inject.Named;

/**
 * An endpoint class we are exposing
 */
@Api(
		name = "newsBeanApi",
		version = "v1",
		resource = "newsBean",
		namespace = @ApiNamespace(
				ownerDomain = "beans.appenginebackend.myapplication.dkrezel.example.com",
				ownerName = "beans.appenginebackend.myapplication.dkrezel.example.com",
				packagePath = ""
		), description = "An API to retrive all news from www.wrocbal.pl"
)
public class NewsBeanEndpoint {

	private static final Logger logger = Logger.getLogger(NewsBeanEndpoint.class.getName());

	@ApiMethod(name = "listNews")
	public List<NewsBean> getAllNewsBean() {
		try {
			Document document = Jsoup.connect("http://wrocbal.pl/wiosna-2015/wlb-a")
					.timeout(5000)
					.get();
			Elements elementsByClass;
			Elements content = document.getElementsByClass("content-body");
			for (Element element : content) {
				elementsByClass = element.getElementsByClass("last-results");
			}

			logger.info("Calling getAllNewsBean method");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new ArrayList<>();
	}
}