package com.example.dkrezel.myapplication.appenginebackend.endpoints;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import pl.teamorganizer.common.Player;

/**
 * An endpoint class we are exposing
 */
@Api(
		name = "playerBeanApi",
		version = "v1",
		resource = "playerBean",
		namespace = @ApiNamespace(
				ownerDomain = "endpoints.appenginebackend.myapplication.dkrezel.example.com",
				ownerName = "endpoints.appenginebackend.myapplication.dkrezel.example.com",
				packagePath = ""
		), description = "Endpoint for parsing data from web site www.wrocbal.pl"
)
public class PlayerBeanEndpoint {

	private static final String URL_KEY = "http://wrocbal.pl";
	private static final String BASE_URL = "http://wrocbal.pl/wiosna-2015/wlb-a/warriors";
	private static final Logger logger = Logger.getLogger(PlayerBeanEndpoint.class.getName());

	private Document document;
	List<Player> playerObjectList;

	@ApiMethod(name = "listPlayers")
	public List<Player> getPlayerList() {
		try {
			document = Jsoup.connect(BASE_URL)
					.timeout(5000)
					.get();
			document.baseUri();
			parseHtml2Json();
			logger.info("Calling getPlayerList method");
			return playerObjectList;
		} catch (IOException e) {
			e.printStackTrace();
			return new ArrayList<>();
		}
	}

	private void parseHtml2Json() {
		Elements playerList = null;
		List<Element> playerNameList = new ArrayList<>();
		List<Element> playerSurnameList = new ArrayList<>();
		List<Element> playerNumberList = new ArrayList<>();
		List<Element> playerPositionList = new ArrayList<>();
		List<Element> playerNumOfMatchList = new ArrayList<>();
		List<Element> playerNumOfGoalsList = new ArrayList<>();
		List<Element> playerAvatarUrlList = new ArrayList<>();
		List<Element> playerTop7List = new ArrayList<>();
		List<Element> playerMVPList = new ArrayList<>();
		List<Element> playerYellowCardList = new ArrayList<>();
		List<Element> playerRedCardList = new ArrayList<>();

		String[] playerNameTab;
		String[] playerSurnameTab;
		String[] playerNumberTab;
		String[] playerPositionTab;
		String[] playerNumOfMatchTab;
		String[] playerNumOfGoalsTab;
		String[] playerAvatarUrlTab;
		String[] playerTop7Tab;
		String[] playerMVPTab;
		String[] playerYellowCardTab;
		String[] playerRedCardTab;

		// Pobierz ze strony HTML tabelę składu drużyny
		Elements squad = document.getElementsByClass("squad");
		for (Element element : squad) {
			// Wybierz ciało tablicy
			playerList = element.getElementsByTag("tbody");
		}

		for (Element element : playerList) {
			// Przeiteruj po wszystkich wierszach i wybierz odpowiednie kolumny
			// ( == pobierz wszystkie elementy według nazwy klasy)
			playerNameList = element.getElementsByClass("col-name");
			playerNumberList = element.getElementsByClass("col-number");
			playerPositionList = element.getElementsByClass("col-position");
			playerNumOfMatchList = element.getElementsByClass("col-games");
			playerNumOfGoalsList = element.getElementsByClass("col-scores");
			playerAvatarUrlList = element.getElementsByClass("col-photo");
			playerTop7List = element.getElementsByClass("col-7");// col-7 - siódemka tygodnia
			playerMVPList = element.getElementsByClass("col-player-of-the-game");// col-player-of-the-game - zawodnik meczu
			playerYellowCardList = element.getElementsByClass("col-ycards");// col-ycards - liczba żółtych kartek
			playerRedCardList = element.getElementsByClass("col-rcards");// col-rcards - liczba czerwonych kartek
		}

		// Przepisz List<Element> na String[]
		playerNameTab = new String[playerNameList.size()];
		playerSurnameTab = new String[playerNameList.size()];
		playerNumberTab = new String[playerNameList.size()];
		playerPositionTab = new String[playerNameList.size()];
		playerNumOfMatchTab = new String[playerNameList.size()];
		playerNumOfGoalsTab = new String[playerNameList.size()];
		playerAvatarUrlTab = new String[playerNameList.size()];
		playerTop7Tab = new String[playerNameList.size()];
		playerMVPTab = new String[playerNameList.size()];
		playerYellowCardTab = new String[playerNameList.size()];
		playerRedCardTab = new String[playerNameList.size()];

		int index = 0;
		for (Element element : playerNameList) {
			String fullName = element.text();
			String[] splittedFullName = fullName.split(" ");
			playerNameTab[index] = splittedFullName[0];
			playerSurnameTab[index++] = splittedFullName[1];
		}

		index = 0;
		for (Element element : playerNumberList) {
			playerNumberTab[index++] = element.text();
		}

		index = 0;
		for (Element element : playerPositionList) {
			playerPositionTab[index++] = element.text();
		}

		index = 0;
		for (Element element : playerNumOfMatchList) {
			playerNumOfMatchTab[index++] = element.text();
		}

		index = 0;
		for (Element element : playerNumOfGoalsList) {
			playerNumOfGoalsTab[index++] = element.text();
		}

		index = 0;
		for (Element element : playerTop7List) {
			playerTop7Tab[index++] = element.text();
		}

		index = 0;
		for (Element element : playerMVPList) {
			playerMVPTab[index++] = element.text();
		}

		index = 0;
		for (Element element : playerYellowCardList) {
			playerYellowCardTab[index++] = element.text();
		}

		index = 0;
		for (Element element : playerRedCardList) {
			playerRedCardTab[index++] = element.text();
		}

		index = 0;
		for (Element element : playerAvatarUrlList) {
			// element.getElementsByTag("div").attr("style")
			String text2Split = element.getElementsByTag("div").attr("style");
			String[] splittedTab = text2Split.split("'");
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append(URL_KEY);
			StringBuilder avatarUrl = stringBuilder.append(splittedTab[1]);
			playerAvatarUrlTab[index++] = avatarUrl.toString();
		}

		playerObjectList = new ArrayList<>(playerNameList.size());
		Player player;
		for (int i = 0; i < playerNameTab.length; i++) {

			player = new Player();
			player.setId(new Long(i + 1));
			player.setName(playerNameTab[i]);
			player.setSurname(playerSurnameTab[i]);
			player.setNumber(playerNumberTab[i]);
			player.setPosition(playerPositionTab[i]);
			player.setGamesPlayed(playerNumOfMatchTab[i]);
			player.setGoals(playerNumOfGoalsTab[i]);
			player.setAvatarURL(playerAvatarUrlTab[i]);
			player.setSevenOfWeek(playerTop7Tab[i]);
			player.setMVP(playerMVPTab[i]);
			player.setYellowCards(playerYellowCardTab[i]);
			player.setRedCards(playerRedCardTab[i]);

			playerObjectList.add(player);
		}
	}
}