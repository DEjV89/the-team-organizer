package com.example.dkrezel.myapplication.appenginebackend.endpoints;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;

import org.joda.time.DateTime;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import pl.teamorganizer.common.Game;

/**
 * Created by dkrezel on 2015-05-23.
 */
@Api(
		name = "upcomingMatchesBeanApi",
		version = "v1",
		resource = "game",
		namespace = @ApiNamespace(
				ownerDomain = "endpoints.appenginebackend.myapplication.dkrezel.example.com",
				ownerName = "endpoints.appenginebackend.myapplication.dkrezel.example.com",
				packagePath = ""
		),
		description = "Endpoint for parsing upcoming matches data from web site www.wrocbal.pl"
)
public class UpcomingMatchesBeanEndpoint {

	private static final Logger logger = Logger.getLogger(UpcomingMatchesBeanEndpoint.class.getName());
	private static final String BASE_URL = "http://wrocbal.pl/wiosna-2015/wlb-a/warriors";

	private Document document;
	private List<Game> upcomingMatchList;

	@ApiMethod(name = "upcomingMatchesList")
	public List<Game> getUpcomingMatches() {

		try {
			document = Jsoup.connect(BASE_URL)
					.timeout(5000)
					.get();
			parseHtml2Json();
			logger.info("Calling upcomingMatchesList method");
			return upcomingMatchList;
		} catch (IOException e) {
			e.printStackTrace();
			return new ArrayList<>();
		}
	}

	private void parseHtml2Json() {
		Elements upcomingMatchesTable = null;
		Elements upcomingMatchesNumber = null;
		List<Elements> matchList;
		Map<Integer, Elements> matchMap;

		//division-incoming-games - pobranie kontenera zwierającego nadchodzące mecze
		Elements elements = document.getElementsByClass("division-incoming-games");
		for (Element element : elements) {
			upcomingMatchesTable = element.getElementsByTag("tbody");
		}

		// liczba nadchodzących meczy
		for (Element element : upcomingMatchesTable) {
			upcomingMatchesNumber = element.getElementsByClass("game");
		}

		int counter = 0;
		matchMap = new HashMap<>(upcomingMatchesTable.size());
		for (Element element : upcomingMatchesNumber) {
			matchMap.put(new Integer(counter++), element.getElementsByTag("td"));
		}

		upcomingMatchList = new ArrayList<>(upcomingMatchesTable.size());
		for (int i = 0; i < upcomingMatchesTable.size(); i++) {
			Game game = new Game();
			Elements gameDetailsMap = matchMap.get(i);

			// Wyciągam zawsze dwa elementy dotyczące meczu
			Element gameDate = gameDetailsMap.get(0);
			Element gameDetails = gameDetailsMap.get(1);

			splitGameDate(gameDate, game);
			splitGameDetails(gameDetails, game);

			upcomingMatchList.add(game);
		}
	}

	private void splitGameDate(Element gameDate, Game game) {
		String[] split = gameDate.text().split(" ");
		String date = split[0];

		String[] splitDate = date.split("\\.");
		String dayOfMonth = splitDate[0];
		String month = splitDate[1];

		String time = split[1];
		String[] splitTime = time.split(":");
		String hour = splitTime[0];
		String minutes = splitTime[1];

		DateTime dateTime = new DateTime(
				2015,
				Integer.parseInt(month),
				Integer.parseInt(dayOfMonth),
				Integer.parseInt(hour),
				Integer.parseInt(minutes)
		);

		game.setDate(dateTime.toDate());
	}

	private void splitGameDetails(Element gameDate, Game game) {
		game.setHomeTeam(gameDate.child(0).text());    // home team
		game.setAwayTeam(gameDate.child(1).text());    // away team
		game.setPlayground(gameDate.child(3).text());    // playground
		game.setScore(" - ");
	}
}
