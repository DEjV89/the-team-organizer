package pl.football.teamorganizer.fragments;

/**
 * Created by dkrezel on 2015-05-17.
 */
public interface FragmentInterface {

	public static final String FRAGMENT_ID = "FRAGMENT_ID";
	public static final String FRAGMENT_LABEL = "FRAGMENT_LABEL";
	public static final int NEWS_FRAGMENT_ID = 0;
	public static final int PLAYERS_FRAGMENT_ID = 1;
	public static final int SCHEDULE_FRAGMENT_ID = 2;
	public static final int MY_GRAPHIC_FRAGMENT_ID = 3;

	public static final String NEWS_FRAGMENT_LABEL = "NEWS_FRAGMENT_LABEL";
	public static final String PLAYERS_FRAGMENT_LABEL = "PLAYERS_FRAGMENT_LABEL";
	public static final String SCHEDULE_FRAGMENT_LABEL = "SCHEDULE_FRAGMENT_LABEL";
	public static final String MY_GRAPHIC_FRAGMENT_LABEL = "MY_GRAPHIC_FRAGMENT_LABEL";
}
