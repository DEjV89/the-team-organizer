package pl.football.teamorganizer.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import pl.football.teamorganizer.R;

/**
 * Created by dkrezel on 2015-05-07.
 */
public class MyFragment extends android.support.v4.app.Fragment {

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View mView = inflater.inflate(R.layout.fragment_layout, container, false);
		TextView mFragmentText = (TextView) mView.findViewById(R.id.fragment_text_id);

		int mInt = getArguments().getInt(FragmentInterface.FRAGMENT_ID);
		String mText = getResources().getStringArray(R.array.activity_names)[mInt];
		mFragmentText.setText(mText);
		getActivity().setTitle(mText);

		return mView;
	}
}
