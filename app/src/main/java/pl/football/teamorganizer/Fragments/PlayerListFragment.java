package pl.football.teamorganizer.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.dkrezel.myapplication.appenginebackend.beans.playerBeanApi.model.Player;

import pl.football.teamorganizer.R;
import pl.football.teamorganizer.activity.PlayerDetailsActivity;
import pl.football.teamorganizer.adapter.fragment.PlayerListFragmentAdapter;
import pl.football.teamorganizer.application.TeamOrganizerApp;

/**
 * Created by dkrezel on 2015-05-17.
 */
public class PlayerListFragment extends android.support.v4.app.Fragment {

	private static final String PLAYER_KEY = "player.key";
	private String[] mPlayerImageUrl;
	private PlayerListFragmentAdapter mPlayerAdapter;
	private TeamOrganizerApp mApplication;
	private ListView mPlayerListView;

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View mView = inflater.inflate(R.layout.players_endpoint_list_layout, container, false);

		mApplication = (TeamOrganizerApp) getActivity().getApplication();
		mPlayerImageUrl = getActivity().getResources().getStringArray(R.array.player_url);
		mPlayerListView = (ListView) mView.findViewById(R.id.player_container_list_id);
		mPlayerAdapter = new PlayerListFragmentAdapter(getActivity(), mApplication.getPlayerListByWebService(), mApplication);
		mPlayerListView.setAdapter(mPlayerAdapter);

		int mFragmentID = getArguments().getInt(FragmentInterface.FRAGMENT_ID);
		String mFragmentTitle = getResources().getStringArray(R.array.activity_names)[mFragmentID];
		getActivity().setTitle(mFragmentTitle);

		mPlayerListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				showPlayerDetails(position);
			}
		});

		return mView;
	}

	private void showPlayerDetails(int position) {
		mApplication.setCurrentPlayer((Player) mPlayerListView.getItemAtPosition(position));
		Intent mPlayerDetailsIntent = new Intent(getActivity(), PlayerDetailsActivity.class);
		startActivity(mPlayerDetailsIntent);
	}
}
