package pl.football.teamorganizer.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import pl.football.teamorganizer.R;
import pl.football.teamorganizer.adapter.fragment.UpcomingMatchAdapter;
import pl.football.teamorganizer.application.TeamOrganizerApp;

/**
 * Created by dkrezel on 2015-05-24.
 */
public class UpcomingMatchFragment extends Fragment {

	private TeamOrganizerApp mApplication;
	private UpcomingMatchAdapter mAdapter;

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.players_endpoint_list_layout, container, false);

		mApplication = (TeamOrganizerApp) getActivity().getApplication();
		ListView listView = (ListView) view.findViewById(R.id.player_container_list_id);
		// utwórz adapter
		mAdapter = new UpcomingMatchAdapter(getActivity(), mApplication.getUpcomingMatchesByWebService());
		// ustaw adapter na liście
		listView.setAdapter(mAdapter);

		return view;
	}
}
