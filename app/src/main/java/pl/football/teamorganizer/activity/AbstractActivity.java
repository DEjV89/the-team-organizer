package pl.football.teamorganizer.activity;

import android.support.v7.app.ActionBarActivity;

/**
 * Created by dkrezel on 2015-04-29.
 */
public abstract class AbstractActivity extends ActionBarActivity {

	protected <T> T findView(int viewId) {
		return (T) findViewById(viewId);
	}
}
