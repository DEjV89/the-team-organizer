package pl.football.teamorganizer.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.dkrezel.myapplication.appenginebackend.beans.playerBeanApi.model.Player;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import pl.football.teamorganizer.R;
import pl.football.teamorganizer.application.TeamOrganizerApp;

/**
 * Created by dkrezel on 2015-05-11.
 */
@Deprecated
public class EndpointActivity extends ActionBarActivity {

	@InjectView(R.id.player_container_list_id)
	protected ListView mPlayerListView;

	private ArrayAdapter<Player> mPlayerAdapter;
	private TeamOrganizerApp mApplication;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.players_endpoint_list_layout);
		ButterKnife.inject(this);

		mApplication = (TeamOrganizerApp) getApplication();

		mPlayerAdapter = new ArrayAdapter<Player>(this, android.R.layout.simple_list_item_1, new ArrayList<Player>());
		mPlayerListView.setAdapter(mPlayerAdapter);
		if (!mApplication.getPlayerListByWebService().isEmpty()) {
			mPlayerAdapter.addAll(mApplication.getPlayerListByWebService());
			mPlayerAdapter.notifyDataSetChanged();
		}

	}

	public ListView getPlayerListView() {
		return mPlayerListView;
	}

	public void setPlayerListView(ListView mPlayerListView) {
		this.mPlayerListView = mPlayerListView;
	}
}
