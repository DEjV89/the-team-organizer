package pl.football.teamorganizer.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBarActivity;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import pl.football.teamorganizer.R;
import pl.football.teamorganizer.tasks.JsoupAsyncTask;

/**
 * Created by dkrezel on 2015-05-14.
 */
@Deprecated
public class JsoupActivity extends ActionBarActivity {

	@InjectView(R.id.jsoup_text_id)
	protected TextView mTextView;

	private TextView mTextView2;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.jsoup_layout);
		ButterKnife.inject(this);

		mTextView2 = (TextView) findViewById(R.id.jsoup_text_id);

		new JsoupAsyncTask(this).execute("http://wrocbal.pl/wiosna-2015/wlb-a/warriors");
	}

	public TextView getTextView() {
		return mTextView;
	}

	public void setTextView(TextView mTextView) {
		this.mTextView = mTextView;
	}
}
