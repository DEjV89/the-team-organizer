package pl.football.teamorganizer.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import br.liveo.interfaces.NavigationLiveoListener;
import br.liveo.navigationliveo.NavigationLiveo;
import butterknife.ButterKnife;
import pl.football.teamorganizer.R;
import pl.football.teamorganizer.fragments.FragmentInterface;
import pl.football.teamorganizer.fragments.MyFragment;
import pl.football.teamorganizer.fragments.PlayerListFragment;
import pl.football.teamorganizer.fragments.UpcomingMatchFragment;

/**
 * Created by dkrezel on 2015-05-01.
 */
public class NavigationFragmentActivity extends NavigationLiveo implements NavigationLiveoListener {

	private android.support.v4.app.FragmentManager mFragmentManager;
	private List<String> mNameItemList;
	private List<Integer> mIconItemList;

	public NavigationFragmentActivity() {
		mNameItemList = new ArrayList<>();
		mIconItemList = new ArrayList<>();
	}

	// Implementacja metod z klasy NavigationLiveo i interfejsu NavigationLiveoListener
	@Override
	public void onUserInformation() {
		// set User information here
		this.mUserName.setText(R.string.author_name);
		this.mUserEmail.setText(R.string.author_email);
		this.mUserPhoto.setImageResource(R.drawable.warriors_logo);
		this.mUserBackground.setImageResource(R.drawable.navigation_background);
	}

	@Override
	public void onInt(Bundle savedInstanceState) {
		this.setNavigationListener(this);
		ButterKnife.inject(this);

		// Lista nawigacyjna w NavigationLiveo
		mNameItemList.add(0, getResources().getStringArray(R.array.activity_names)[0]);
		mNameItemList.add(1, getResources().getStringArray(R.array.activity_names)[1]);
		mNameItemList.add(2, getResources().getStringArray(R.array.activity_names)[2]);
		mNameItemList.add(3, getResources().getStringArray(R.array.activity_names)[3]);

		// Lista ikon prezentowanych przy pozycjach listy nawigacyjnej
		mIconItemList.add(0, R.drawable.abc_ic_menu_copy_mtrl_am_alpha);
		mIconItemList.add(1, R.drawable.abc_ic_menu_copy_mtrl_am_alpha);
		mIconItemList.add(2, R.drawable.abc_ic_menu_copy_mtrl_am_alpha);
		mIconItemList.add(3, R.drawable.abc_ic_menu_copy_mtrl_am_alpha);

		this.setFooterInformationDrawer(R.string.menu_name, R.drawable.abc_ic_menu_cut_mtrl_alpha);
		this.setNavigationAdapter(mNameItemList, mIconItemList, null, null);
		this.setDefaultStartPositionNavigation(0);    // ustawia domyślnie ustawioną pozycję w NavigationDrawer

		mFragmentManager = getSupportFragmentManager();
		mFragmentManager.addOnBackStackChangedListener(new android.support.v4.app.FragmentManager.OnBackStackChangedListener() {
			@Override
			public void onBackStackChanged() {

			}
		});
	}

	@Override
	public void onItemClickNavigation(int position, int layoutContainerId) {
		// Wykorzystanie FragmentManager'a do podmiany Fragmentów
		mFragmentManager = getSupportFragmentManager();
		//Tworzenie nowego fragmentu, przekazaniu mu argumentów w postaci Bundle
		Fragment mFragment;
		Bundle bundle = new Bundle();
		bundle.putInt(FragmentInterface.FRAGMENT_ID, position);    // identyfikator pozycji danego fragmentu
		switch (position) {
			case FragmentInterface.NEWS_FRAGMENT_ID:
				mFragment = new MyFragment();
				bundle.putString(FragmentInterface.FRAGMENT_LABEL, FragmentInterface.NEWS_FRAGMENT_LABEL);
				break;
			case FragmentInterface.PLAYERS_FRAGMENT_ID:
				mFragment = new PlayerListFragment();
				bundle.putString(FragmentInterface.FRAGMENT_LABEL, FragmentInterface.PLAYERS_FRAGMENT_LABEL);
				break;
			case FragmentInterface.SCHEDULE_FRAGMENT_ID:
				mFragment = new MyFragment();
				bundle.putString(FragmentInterface.FRAGMENT_LABEL, FragmentInterface.SCHEDULE_FRAGMENT_LABEL);
				break;
			case FragmentInterface.MY_GRAPHIC_FRAGMENT_ID:
				mFragment = new UpcomingMatchFragment();
				bundle.putString(FragmentInterface.FRAGMENT_LABEL, FragmentInterface.MY_GRAPHIC_FRAGMENT_LABEL);
				break;
			default:
				mFragment = new MyFragment();
		}
		mFragment.setArguments(bundle);

		if (mFragmentManager.getFragments() != null && !mFragmentManager.getFragments().isEmpty()) {
			mFragmentManager.beginTransaction()
					.replace(layoutContainerId, mFragment, mFragment.getArguments().getString(FragmentInterface.FRAGMENT_LABEL))
					.addToBackStack(null)
					.commit();
		} else {
			mFragmentManager.beginTransaction()
					.replace(layoutContainerId, mFragment, mFragment.getArguments().getString(FragmentInterface.FRAGMENT_LABEL))
					.commit();
		}
	}

	@Override
	public void onPrepareOptionsMenuNavigation(Menu menu, int position, boolean visible) {

		switch (position) {
			case 0:
				menu.findItem(R.id.team_menu).setVisible(!visible);
				break;
			case 1:
				menu.findItem(R.id.team_menu).setVisible(!visible);
				break;
		}
	}

	@Override
	public void onClickFooterItemNavigation(View view) {
		view.isSelected();
	}

	@Override
	public void onClickUserPhotoNavigation(View view) {
		view.isSelected();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_layout, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int mItemId = item.getItemId();

		if (mItemId == R.id.settings_id) {
			Toast.makeText(this, R.string.not_implemented, Toast.LENGTH_SHORT).show();
			return true;
		} else {
			return super.onOptionsItemSelected(item);
		}
	}
}
