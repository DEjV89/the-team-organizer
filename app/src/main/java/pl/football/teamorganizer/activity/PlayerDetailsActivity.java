package pl.football.teamorganizer.activity;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dkrezel.myapplication.appenginebackend.beans.playerBeanApi.model.Player;
import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.InjectView;
import pl.football.teamorganizer.R;
import pl.football.teamorganizer.application.TeamOrganizerApp;

/**
 * Created by dkrezel on 2015-05-01.
 */
public class PlayerDetailsActivity extends Activity {

	private TeamOrganizerApp mApplication;

	@InjectView(R.id.player_details_avatar_id)
	protected ImageView mPlayerAvatar;

	@InjectView(R.id.player_details_full_name_id)
	protected TextView mPlayerFullName;

	@InjectView(R.id.player_details_number_id)
	protected TextView mPlayerNumber;

	@InjectView(R.id.player_details_position_id)
	protected TextView mPlayerPosition;

	@InjectView(R.id.player_details_games_id)
	protected TextView mPlayerGamesPlayed;

	@InjectView(R.id.player_details_golas_id)
	protected TextView mPlayerGoals;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView((R.layout.player_details_layout));
		ButterKnife.inject(this);

		mApplication = (TeamOrganizerApp) getApplication();

		unpackPlayer();
	}

	private void unpackPlayer() {
		Player mPlayer = mApplication.getCurrentPlayer();
		Picasso.with(this)
				.load(mPlayer.getAvatarURL())
				.resize(300, 300)
				.centerCrop()
				.placeholder(R.drawable.warriors_logo)
				.into(mPlayerAvatar);
		mPlayerFullName.setText(mPlayer.getName() + " " + mPlayer.getSurname());
		mPlayerNumber.setText(mPlayer.getNumber());
		mPlayerPosition.setText(mPlayer.getPosition());
		mPlayerGamesPlayed.setText(mPlayer.getGamesPlayed());
		mPlayerGoals.setText(mPlayer.getGoals().toString());
	}
}
