package pl.football.teamorganizer.activity;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import pl.football.teamorganizer.R;
import pl.football.teamorganizer.adapter.PlayersListAdapter;
import pl.football.teamorganizer.application.TeamOrganizerApp;
import pl.football.teamorganizer.model.Player;

/**
 * Created by dkrezel on 2015-04-29.
 */
@Deprecated
public class PlayersActivity extends ListActivity {

	private ListView mPlayersView;
	private List<Player> mPlayersList = new ArrayList<>();
	private TeamOrganizerApp mApp;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.players_list_view);

		mApp = (TeamOrganizerApp) getApplication();

		loadPlayersList();
		PlayersListAdapter mPlayersListAdapter = new PlayersListAdapter(this, mPlayersList, getApplication());

		setListAdapter(mPlayersListAdapter);

	}

	@Override
	protected void onListItemClick(ListView listView, View view, int position, long id) {
		mApp.setCurrentPlayerAvatar(mApp.getPlayerAvatarMap().get(new Long(position)));
		Intent mPlayerDetailsIntent = new Intent(this, PlayerDetailsActivity.class);
		startActivity(mPlayerDetailsIntent);
	}

	private void loadPlayersList() {
		mPlayersList.add(new Player(0L, "Dawid", "Krężel", "Rozgrywający", null));
		mPlayersList.add(new Player(1L, "Andrzej", "Zioła", "Obrońca", null));
		mPlayersList.add(new Player(2L, "Damian", "Malicha", "Rozgrywający", null));
		mPlayersList.add(new Player(3L, "Grzegorz", "Malicha", "Napastnik", null));
		mPlayersList.add(new Player(4L, "Oleksander", "Topolnystkyy", "Napastnik", null));
		mPlayersList.add(new Player(5L, "Marcin", "Wcisło", "Rozgrywający", null));
		mPlayersList.add(new Player(6L, "Maciek", "Setkowicz", "Obrońca", null));
		mPlayersList.add(new Player(7L, "Krzysztof", "Czyżykiewicz", "Obrońca", null));
	}

	protected <T> T findView(int viewId) {
		return (T) findViewById(viewId);
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_layout, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int mItemId = item.getItemId();

		return true;
	}
}
