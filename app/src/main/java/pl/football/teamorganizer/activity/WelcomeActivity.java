package pl.football.teamorganizer.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import java.util.Timer;
import java.util.TimerTask;

import pl.football.teamorganizer.R;
import pl.football.teamorganizer.application.TeamOrganizerApp;
import pl.football.teamorganizer.tasks.PlayerInfoListAsyncTask;
import pl.football.teamorganizer.tasks.UpcomingMatchesAsyncTask;

/**
 * Created by dkrezel on 2015-05-16.
 */
public class WelcomeActivity extends ActionBarActivity {

	public static final int WELCOME_SCHEDULER_DELAY = 2000;

	private TeamOrganizerApp mApplication;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.welcome_layout);

		mApplication = (TeamOrganizerApp) getApplication();

		// Uruchomienie zadania w AsyncTask do pobrania listy zawodników z webSerwisu
		new PlayerInfoListAsyncTask(mApplication).execute();
		new UpcomingMatchesAsyncTask(mApplication).execute();

		new Timer().schedule(new TimerTask() {
			@Override
			public void run() {
				process();
			}
		}, WELCOME_SCHEDULER_DELAY);
	}

	private void process() {
		if (this.isFinishing()) {
			return;
		}
		startActivity(new Intent(WelcomeActivity.this, NavigationFragmentActivity.class));
		finish();
	}
}
