package pl.football.teamorganizer.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import pl.football.teamorganizer.model.NavigationDrawerItem;

/**
 * Created by dkrezel on 2015-05-07.
 */
@Deprecated
public class NavigationDrawerAdapter extends ArrayAdapter<NavigationDrawerItem> {

	private LayoutInflater mInflater;
	private List<NavigationDrawerItem> mNavigationItemList;

	public NavigationDrawerAdapter(Context mContext, List<NavigationDrawerItem> mData) {
		super(mContext, android.R.layout.simple_list_item_1, mData);
		mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mNavigationItemList = new ArrayList<>(mData);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		FragmentViewHolder mViewHolder;
		if (convertView == null) {
			convertView = mInflater.inflate(android.R.layout.simple_list_item_1, parent, false);
			mViewHolder = new FragmentViewHolder();
			NavigationDrawerItem item = getItem(position);
			TextView mTextView = (TextView) convertView.findViewById(android.R.id.text1);
			mViewHolder.mHolderTextView = mTextView;
			mViewHolder.mHolderNavigationDrawerItem = item;

			convertView.setTag(mViewHolder);
			mTextView.setText(item.getLabel());

		} else {
			mViewHolder = (FragmentViewHolder) convertView.getTag();
		}
		return convertView;
	}

	private class FragmentViewHolder {
		public NavigationDrawerItem mHolderNavigationDrawerItem;
		public TextView mHolderTextView;
	}
}
