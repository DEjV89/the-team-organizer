package pl.football.teamorganizer.adapter;

import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import pl.football.teamorganizer.R;
import pl.football.teamorganizer.application.TeamOrganizerApp;
import pl.football.teamorganizer.model.Player;
import pl.football.teamorganizer.tasks.DownloadPlayerImageAsyncTask;

/**
 * Created by dkrezel on 2015-04-29.
 */
@Deprecated
public class PlayersListAdapter extends ArrayAdapter<Player> {

	public static final String TAG = PlayersListAdapter.class.getName();

	private LayoutInflater mInflater;
	private List<String> mPlayerElement;
	private String[] mPlayerImageUrl;
	private TeamOrganizerApp mApp;

	public PlayersListAdapter(Context mContext, List<Player> mData, Application mApplication) {
		super(mContext, R.layout.player_element_list, mData);
		mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mPlayerElement = new ArrayList<>(mData.size());
		mPlayerImageUrl = mContext.getResources().getStringArray(R.array.player_url);
		mApp = (TeamOrganizerApp) mApplication;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Log.d(TAG, "getView");
		PlayerHolder mPlayerHolder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.player_element_list, parent, false);
			mPlayerHolder = new PlayerHolder();

			TextView mNameView = (TextView) convertView.findViewById(R.id.player_full_name_id);
			ImageView mPlayerView = (ImageView) convertView.findViewById(R.id.player_avatar_id);
			Player mPlayer = getItem(position);
			mPlayerHolder.mPlayerName = mNameView;
			mPlayerHolder.mPlayerView = mPlayerView;
			mPlayerHolder.mPlayer = mPlayer;
			convertView.setTag(mPlayerHolder);

			mPlayerHolder.mPlayerName.setText(mPlayer.getName());
			mPlayerHolder.mPlayerView.setImageDrawable(null);
			mPlayerHolder.mPlayerView.setTag(position);
			String mPlayerUrl = this.mPlayerImageUrl[position];
			new DownloadPlayerImageAsyncTask(position, mPlayerHolder.mPlayerView, mApp).execute(mPlayerUrl);
		} else {
			mPlayerHolder = (PlayerHolder) convertView.getTag();
		}

		return convertView;
	}

	public static class PlayerHolder {
		public Player mPlayer;
		public TextView mPlayerName;
		public TextView mPlayerPosition;
		public ImageView mPlayerView;
		public Bitmap mAvatar;
	}
}
