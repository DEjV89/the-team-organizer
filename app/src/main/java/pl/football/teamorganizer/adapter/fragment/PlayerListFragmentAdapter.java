package pl.football.teamorganizer.adapter.fragment;

import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dkrezel.myapplication.appenginebackend.beans.playerBeanApi.model.Player;
import com.squareup.picasso.Picasso;

import java.util.List;

import pl.football.teamorganizer.R;
import pl.football.teamorganizer.application.TeamOrganizerApp;

/**
 * Created by dkrezel on 2015-05-17.
 */
public class PlayerListFragmentAdapter extends ArrayAdapter<Player> {

	private TeamOrganizerApp mApplication;
	private LayoutInflater mInflater;
	private Context mContext;

	public PlayerListFragmentAdapter(Context mContext, List<Player> mData, Application mApplication) {
		super(mContext, R.layout.player_element_list, mData);
		this.mApplication = (TeamOrganizerApp) mApplication;
		mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.mContext = mContext;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		PlayerHolder mPlayerHolder;
		Player mPlayerItem = getItem(position);
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.player_element_list, parent, false);
			mPlayerHolder = new PlayerHolder();

			TextView mPlayerFullName = (TextView) convertView.findViewById(R.id.player_full_name_id);
			TextView mPlayerPosition = (TextView) convertView.findViewById(R.id.player_position_id);
			TextView mPlayerGamesPlayed = (TextView) convertView.findViewById(R.id.player_games_id);
			ImageView mPlayerAvatar = (ImageView) convertView.findViewById(R.id.player_avatar_id);

			mPlayerHolder.mPlayerFullName = mPlayerFullName;
			mPlayerHolder.mPlayerPosition = mPlayerPosition;
			mPlayerHolder.mPlayerGamesPlayed = mPlayerGamesPlayed;
			mPlayerHolder.mPlayerAvatar = mPlayerAvatar;
			mPlayerHolder.mPlayerAvatar.setTag(position);

			convertView.setTag(mPlayerHolder);
		} else {
			mPlayerHolder = (PlayerHolder) convertView.getTag();
		}

		mPlayerHolder.mPlayerFullName.setText(mPlayerItem.getName() + " " + mPlayerItem.getSurname());
		mPlayerHolder.mPlayerPosition.setText(mPlayerItem.getPosition());
		mPlayerHolder.mPlayerGamesPlayed.setText(mPlayerItem.getGamesPlayed());
		Picasso.with(mContext)
				.load(mPlayerItem.getAvatarURL())
				.resize(300, 300)
				.centerCrop()
				.placeholder(R.drawable.warriors_logo)
				.into(mPlayerHolder.mPlayerAvatar);

		return convertView;
	}

	private static class PlayerHolder {
		public Player mPlayer;
		public String mPlayerAvatarURL;
		public TextView mPlayerFullName;
		public TextView mPlayerPosition;
		public TextView mPlayerGamesPlayed;
		public ImageView mPlayerAvatar;
		public Bitmap mAvatar;

	}
}
