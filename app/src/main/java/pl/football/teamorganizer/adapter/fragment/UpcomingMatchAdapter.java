package pl.football.teamorganizer.adapter.fragment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import common.teamorganizer.common.upcomingMatchesBeanApi.model.Game;
import pl.football.teamorganizer.R;

/**
 * Created by dkrezel on 2015-05-24.
 */
public class UpcomingMatchAdapter extends ArrayAdapter<Game> {

	private LayoutInflater mInflater;
	private Context mContext;

	public UpcomingMatchAdapter(Context context, List<Game> data) {
		super(context, R.layout.upcoming_match_item, data);
		mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mContext = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Game item = getItem(position);
		GameHolder gameHolder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.upcoming_match_item, parent, false);
			gameHolder = new GameHolder();

			ImageView homeTeamImage = (ImageView) convertView.findViewById(R.id.home_team_arms_id);
			ImageView awayTeamImage = (ImageView) convertView.findViewById(R.id.away_team_arms_id);
			TextView homeTeamName = (TextView) convertView.findViewById(R.id.home_team_name_id);
			TextView awayTeamName = (TextView) convertView.findViewById(R.id.away_team_name_id);
			TextView matchDate = (TextView) convertView.findViewById(R.id.match_date_id);

			gameHolder.homeTeamImage = homeTeamImage;
			gameHolder.awayTeamImage = awayTeamImage;
			gameHolder.homeTeamName = homeTeamName;
			gameHolder.awayTeamName = awayTeamName;
			gameHolder.matchDate = matchDate;

			convertView.setTag(gameHolder);
		} else {
			gameHolder = (GameHolder) convertView.getTag();
		}

		gameHolder.homeTeamName.setText(item.getHomeTeam());
		gameHolder.awayTeamName.setText(item.getAwayTeam());
		gameHolder.matchDate.setText(item.getDate().toString());

		return convertView;
	}

	private static class GameHolder {
		public ImageView homeTeamImage;
		public ImageView awayTeamImage;
		public TextView homeTeamName;
		public TextView awayTeamName;
		public TextView matchDate;
	}
}
