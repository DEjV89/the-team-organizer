package pl.football.teamorganizer.application;

import android.app.Application;
import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import common.teamorganizer.common.upcomingMatchesBeanApi.model.Game;
import pl.football.teamorganizer.R;

/**
 * Created by dkrezel on 2015-05-06.
 */
public class TeamOrganizerApp extends Application {

	public static String[] mPlayerImageUrl;
	private Map<Long, Bitmap> mPlayerAvatarMap;
	private com.example.dkrezel.myapplication.appenginebackend.beans.playerBeanApi.model.Player mCurrentPlayer;
	private Bitmap mCurrentPlayerAvatar;
	private List<com.example.dkrezel.myapplication.appenginebackend.beans.playerBeanApi.model.Player> mPlayerListByWebService;
	private List<Game> mUpcomingMatchesByWebService;

	@Override
	public void onCreate() {
		super.onCreate();
		mPlayerAvatarMap = new HashMap<>(8);
		mCurrentPlayer = null;
		mPlayerListByWebService = new ArrayList<>();
		mUpcomingMatchesByWebService = new ArrayList<>();
		mPlayerImageUrl = getResources().getStringArray(R.array.player_url);
	}

	@Override
	public void onTerminate() {
		super.onTerminate();
	}

	public Map<Long, Bitmap> getPlayerAvatarMap() {
		return mPlayerAvatarMap;
	}

	public com.example.dkrezel.myapplication.appenginebackend.beans.playerBeanApi.model.Player getCurrentPlayer() {
		return mCurrentPlayer;
	}

	public void setCurrentPlayer(com.example.dkrezel.myapplication.appenginebackend.beans.playerBeanApi.model.Player mCurrentPlayer) {
		this.mCurrentPlayer = mCurrentPlayer;
	}

	public Bitmap getCurrentPlayerAvatar() {
		return mCurrentPlayerAvatar;
	}

	public void setCurrentPlayerAvatar(Bitmap mCurrentPlayerAvatar) {
		this.mCurrentPlayerAvatar = mCurrentPlayerAvatar;
	}

	public List<com.example.dkrezel.myapplication.appenginebackend.beans.playerBeanApi.model.Player> getPlayerListByWebService() {
		return mPlayerListByWebService;
	}

	public void setPlayerListByWebService(List<com.example.dkrezel.myapplication.appenginebackend.beans.playerBeanApi.model.Player> mPlayerListByWebService) {
		this.mPlayerListByWebService = mPlayerListByWebService;
	}

	public List<Game> getUpcomingMatchesByWebService() {
		return mUpcomingMatchesByWebService;
	}

	public void setUpcomingMatchesByWebService(List<Game> mUpcomingMatchesByWebService) {
		this.mUpcomingMatchesByWebService = mUpcomingMatchesByWebService;
	}
}
