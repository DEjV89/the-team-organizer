package pl.football.teamorganizer.model;

/**
 * Created by dkrezel on 2015-05-07.
 */
public class NavigationDrawerItem {

	private Long id;
	private String label;

	public NavigationDrawerItem(Long id, String label) {
		this.id = id;
		this.label = label;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
}
