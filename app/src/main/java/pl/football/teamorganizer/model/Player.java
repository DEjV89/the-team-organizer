package pl.football.teamorganizer.model;

import android.graphics.Bitmap;

/**
 * Created by dkrezel on 2015-05-06.
 */
@Deprecated
public class Player {

	private Long id;
	private String name;
	private String surname;
	private String playingPosition;
	private Bitmap avatar;

	public Player() {
		id = 0L;
		name = null;
		surname = null;
		playingPosition = null;
		avatar = null;
	}

	public Player(Long id, String name, String surname, String playingPosition, Bitmap avatar) {
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.playingPosition = playingPosition;
		this.avatar = avatar;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getPlayingPosition() {
		return playingPosition;
	}

	public void setPlayingPosition(String playingPosition) {
		this.playingPosition = playingPosition;
	}

	public Bitmap getAvatar() {
		return avatar;
	}

	public void setAvatar(Bitmap avatar) {
		this.avatar = avatar;
	}
}
