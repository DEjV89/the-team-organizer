package pl.football.teamorganizer.tasks;

import android.app.Application;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import java.net.URL;
import java.net.URLConnection;

import pl.football.teamorganizer.application.TeamOrganizerApp;

/**
 * Created by dkrezel on 2015-05-05.
 */
@Deprecated
public class DownloadPlayerImageAsyncTask extends AsyncTask<String, Void, Bitmap> {

	public static final String TAG = DownloadPlayerImageAsyncTask.class.getName();

	private int mPosition;
	private ImageView mPlayerView;
	private Drawable mPlaceholder;
	private TeamOrganizerApp mApplication;

	public DownloadPlayerImageAsyncTask(int mPosition, ImageView mPlayerView, Application mApplication) {
		this.mPosition = mPosition;
		this.mPlayerView = mPlayerView;
		// pobierz zasoby, ustaw domyślny obrazek z zasobów Android'a
		Resources mResource = mPlayerView.getContext().getResources();
		this.mPlaceholder = mResource.getDrawable(android.R.drawable.gallery_thumb);
		this.mApplication = (TeamOrganizerApp) mApplication;
	}

	@Override
	protected void onPreExecute() {
		mPlayerView.setImageDrawable(mPlaceholder);
	}

	@Override
	protected Bitmap doInBackground(String... inputUrls) {
		try {
			Log.d(TAG, "doInBackground " + Thread.currentThread().getId());
			URL mUrl = new URL(inputUrls[0]);
			URLConnection conn = mUrl.openConnection();
			conn.setConnectTimeout(3000);
			conn.setReadTimeout(5000);
			return BitmapFactory.decodeStream(conn.getInputStream());
		} catch (java.io.IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	protected void onPostExecute(Bitmap bitmap) {
		int mForPosition = (Integer) mPlayerView.getTag();
		if (mForPosition == this.mPosition) {
			this.mPlayerView.setImageBitmap(bitmap);
			mApplication.getPlayerAvatarMap().put(new Long(this.mPosition), bitmap);
		}

	}
}
