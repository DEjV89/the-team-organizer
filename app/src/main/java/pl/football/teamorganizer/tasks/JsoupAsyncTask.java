package pl.football.teamorganizer.tasks;

import android.os.AsyncTask;
import android.widget.Toast;

import com.google.api.client.json.Json;
import com.google.api.client.json.JsonGenerator;

import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;

import pl.football.teamorganizer.activity.JsoupActivity;

/**
 * Created by dkrezel on 2015-05-15.
 */
@Deprecated
public class JsoupAsyncTask extends AsyncTask<String, Void, Document> {

	public static final String PLAYER_ID_KEY = "id";
	public static final String PLAYER_NAME_KEY = "fullName";
	public static final String PLAYER_NUMBER_KEY = "number";
	public static final String PLAYER_POSITION_KEY = "position";
	public static final String PLAYER_GAMES_PLAYED_KEY = "games";

	private JsoupActivity jsoupActivity;

	public JsoupAsyncTask(JsoupActivity jsoupActivity) {
		this.jsoupActivity = jsoupActivity;
	}

	@Override
	protected Document doInBackground(String... params) {
		try {
			Document mDocument = Jsoup.connect(params[0]).timeout(5000).get();
			return mDocument;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected void onPostExecute(Document document) {
		Elements playerList = null;
		List<Element> playerNameList = new ArrayList<>();
		List<Element> playerNumberList = new ArrayList<>();
		List<Element> playerPositionList = new ArrayList<>();
		List<Element> playerNumOfMatchList = new ArrayList<>();

		String[] playerNameTab;
		String[] playerNumberTab;
		String[] playerPositionTab;
		String[] playerNumOfMatchTab;

		JsonObjectBuilder jsonBuilder = javax.json.Json.createObjectBuilder();
		JsonObjectBuilder playerBuilder = javax.json.Json.createObjectBuilder();
		JsonArrayBuilder playerArrayBuilder = javax.json.Json.createArrayBuilder();

		JsonObject jsonResult = null;

		Elements squad = document.getElementsByClass("squad");
		for (Element element : squad) {
			playerList = element.getElementsByTag("tbody");
		}

		for (Element element : playerList) {
			playerNameList = element.getElementsByClass("col-name");
			playerNumberList = element.getElementsByClass("col-number");
			playerPositionList = element.getElementsByClass("col-position");
			playerNumOfMatchList = element.getElementsByClass("col-games");
		}

		playerNameTab = new String[playerNameList.size()];
		playerNumberTab = new String[playerNameList.size()];
		playerPositionTab = new String[playerNameList.size()];
		playerNumOfMatchTab = new String[playerNameList.size()];

		int index = 0;
		for (Element element : playerNameList) {
			playerNameTab[index++] = element.text();
		}

		index = 0;
		for (Element element : playerNumberList) {
			playerNumberTab[index++] = element.text();
		}

		index = 0;
		for (Element element : playerPositionList) {
			playerPositionTab[index++] = element.text();
		}

		index = 0;
		for (Element element : playerNumOfMatchList) {
			playerNumOfMatchTab[index++] = element.text();
		}

		for (int i = 0; i < playerNameTab.length; i++) {
			playerBuilder.add(PLAYER_ID_KEY, i+1);
			playerBuilder.add(PLAYER_NAME_KEY, playerNameTab[i]);
			playerBuilder.add(PLAYER_NUMBER_KEY, playerNumberTab[i]);
			playerBuilder.add(PLAYER_POSITION_KEY, playerPositionTab[i]);
			playerBuilder.add(PLAYER_GAMES_PLAYED_KEY, playerNumOfMatchTab[i]);
			playerArrayBuilder.add(playerBuilder);
		}
		jsonResult = jsonBuilder.add("players", playerArrayBuilder).build();

		Collection<JsonValue> values = jsonResult.values();

		jsoupActivity.getTextView().setText(jsonResult.toString());
	}
}
