package pl.football.teamorganizer.tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.example.dkrezel.myapplication.appenginebackend.beans.playerBeanApi.PlayerBeanApi;
import com.example.dkrezel.myapplication.appenginebackend.beans.playerBeanApi.model.PlayerCollection;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.extensions.android.json.AndroidJsonFactory;

import java.io.IOException;

import pl.football.teamorganizer.application.TeamOrganizerApp;

/**
 * Created by dkrezel on 2015-05-15.
 */
public class PlayerInfoListAsyncTask extends AsyncTask<Void, Void, PlayerCollection> {

	private static final String TAG = "PlayerInfoListAsyncTask";

	private TeamOrganizerApp mApplication;
	private PlayerBeanApi playerBeanApi = null;

	public PlayerInfoListAsyncTask(TeamOrganizerApp mApplication) {
		this.mApplication = mApplication;
	}

	@Override
	protected PlayerCollection doInBackground(Void... params) {
		if (playerBeanApi == null) {
			PlayerBeanApi.Builder mBuilder = new PlayerBeanApi.Builder(AndroidHttp.newCompatibleTransport(), new AndroidJsonFactory(), null)
					.setRootUrl("https://maximal-yew-94514.appspot.com/_ah/api/");
			playerBeanApi = mBuilder.build();
		}
		try {
			return playerBeanApi.listPlayers().execute();
		} catch (IOException e) {
			e.printStackTrace();
			return new PlayerCollection();
		}
	}

	@Override
	protected void onPostExecute(PlayerCollection playerCollection) {
		Log.d(TAG, "onPostExecute PlayerInfoListAsyncTask");
		// Zapisanie pobranej listy Zawodników w instancji applikacji
		mApplication.setPlayerListByWebService(playerCollection.getItems());
	}
}
