package pl.football.teamorganizer.tasks;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.extensions.android.json.AndroidJsonFactory;

import java.io.IOException;

import common.teamorganizer.common.upcomingMatchesBeanApi.UpcomingMatchesBeanApi;
import common.teamorganizer.common.upcomingMatchesBeanApi.model.GameCollection;
import pl.football.teamorganizer.application.TeamOrganizerApp;

/**
 * Created by dkrezel on 2015-05-24.
 */
public class UpcomingMatchesAsyncTask extends AsyncTask<Void, Void, GameCollection> {

	private static final String TAG = "UpcomingMatchesAsyncTask";

	private TeamOrganizerApp application;
	private UpcomingMatchesBeanApi upcomingMatchesBeanApi;

	public UpcomingMatchesAsyncTask(Application application) {
		this.application = (TeamOrganizerApp) application;
	}

	@Override
	protected GameCollection doInBackground(Void... params) {
		if (upcomingMatchesBeanApi == null) {
			UpcomingMatchesBeanApi.Builder builder = new UpcomingMatchesBeanApi.Builder(AndroidHttp.newCompatibleTransport(), new AndroidJsonFactory(), null)
					.setRootUrl("https://maximal-yew-94514.appspot.com/_ah/api/");
			upcomingMatchesBeanApi = builder.build();
		}
		try {
			return upcomingMatchesBeanApi.upcomingMatchesList().execute();
		} catch (IOException e) {
			e.printStackTrace();
			return new GameCollection();
		}
	}

	@Override
	protected void onPostExecute(GameCollection gameCollection) {
		Log.d(TAG, "onPostExecute UpcomingMatchesAsync");
		application.setUpcomingMatchesByWebService(gameCollection.getItems());
	}
}
