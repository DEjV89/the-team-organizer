package pl.teamorganizer.common;

import java.util.Date;

/**
 * Created by dkrezel on 2015-05-23.
 */
public class Game {

	private Date date;
	private String homeTeam;
	private String homeTeamArmsURL;
	private String awayTeam;
	private String awayTeamArmsURL;
	private String playground;
	private String score;

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getHomeTeam() {
		return homeTeam;
	}

	public void setHomeTeam(String homeTeam) {
		this.homeTeam = homeTeam;
	}

	public String getAwayTeam() {
		return awayTeam;
	}

	public void setAwayTeam(String awayTeam) {
		this.awayTeam = awayTeam;
	}

	public String getPlayground() {
		return playground;
	}

	public void setPlayground(String playground) {
		this.playground = playground;
	}

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	public String getHomeTeamArmsURL() {
		return homeTeamArmsURL;
	}

	public void setHomeTeamArmsURL(String homeTeamArmsURL) {
		this.homeTeamArmsURL = homeTeamArmsURL;
	}

	public String getAwayTeamArmsURL() {
		return awayTeamArmsURL;
	}

	public void setAwayTeamArmsURL(String awayTeamArmsURL) {
		this.awayTeamArmsURL = awayTeamArmsURL;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Game game = (Game) o;

		if (!awayTeam.equals(game.awayTeam)) return false;
		if (awayTeamArmsURL != null ? !awayTeamArmsURL.equals(game.awayTeamArmsURL) : game.awayTeamArmsURL != null)
			return false;
		if (!date.equals(game.date)) return false;
		if (!homeTeam.equals(game.homeTeam)) return false;
		if (homeTeamArmsURL != null ? !homeTeamArmsURL.equals(game.homeTeamArmsURL) : game.homeTeamArmsURL != null)
			return false;
		if (!playground.equals(game.playground)) return false;
		if (score != null ? !score.equals(game.score) : game.score != null) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = date.hashCode();
		result = 31 * result + homeTeam.hashCode();
		result = 31 * result + (homeTeamArmsURL != null ? homeTeamArmsURL.hashCode() : 0);
		result = 31 * result + awayTeam.hashCode();
		result = 31 * result + (awayTeamArmsURL != null ? awayTeamArmsURL.hashCode() : 0);
		result = 31 * result + playground.hashCode();
		result = 31 * result + (score != null ? score.hashCode() : 0);
		return result;
	}
}
