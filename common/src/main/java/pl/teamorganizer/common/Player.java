package pl.teamorganizer.common;

/**
 * Created by dkrezel on 2015-05-16.
 */
public class Player implements Comparable<Player> {

	private long id;
	private String number;
	private String name;
	private String surname;
	private String position;
	private String sevenOfWeek;
	private String MVP;
	private String gamesPlayed;
	private String goals;
	private String yellowCards;
	private String redCards;
	private String avatarURL;


	public Player() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getGamesPlayed() {
		return gamesPlayed;
	}

	public void setGamesPlayed(String gamesPlayed) {
		this.gamesPlayed = gamesPlayed;
	}

	public String getAvatarURL() {
		return avatarURL;
	}

	public void setAvatarURL(String avatarURL) {
		this.avatarURL = avatarURL;
	}

	public String getGoals() {
		return goals;
	}

	public void setGoals(String goals) {
		this.goals = goals;
	}

	public String getSevenOfWeek() {
		return sevenOfWeek;
	}

	public void setSevenOfWeek(String sevenOfWeek) {
		this.sevenOfWeek = sevenOfWeek;
	}

	public String getMVP() {
		return MVP;
	}

	public void setMVP(String MVP) {
		this.MVP = MVP;
	}

	public String getYellowCards() {
		return yellowCards;
	}

	public void setYellowCards(String yellowCards) {
		this.yellowCards = yellowCards;
	}

	public String getRedCards() {
		return redCards;
	}

	public void setRedCards(String redCards) {
		this.redCards = redCards;
	}

	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(name)
				.append(" ")
				.append(surname)
				.append("\n")
				.append("Pozycja : ")
				.append(position)
				.append("\n")
				.append("Liczba rozegranych meczy : ")
				.append("\n")
				.append(gamesPlayed);
		return stringBuilder.toString();
	}

	@Override
	public int compareTo(Player o) {
		// Porównanie obiektów względem nazwiska i imienia.
		String surname = o.getSurname();
		String name = o.getName();
		if (this.surname.compareTo(surname) == 0) {
			return this.name.compareTo(name);
		} else {
			return this.surname.compareTo(surname);
		}
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Player player = (Player) o;

		if (id != player.id) return false;
		if (MVP != null ? !MVP.equals(player.MVP) : player.MVP != null) return false;
		if (!surname.equals(player.surname)) return false;
		if (avatarURL != null ? !avatarURL.equals(player.avatarURL) : player.avatarURL != null)
			return false;
		if (gamesPlayed != null ? !gamesPlayed.equals(player.gamesPlayed) : player.gamesPlayed != null)
			return false;
		if (goals != null ? !goals.equals(player.goals) : player.goals != null) return false;
		if (!name.equals(player.name)) return false;
		if (!number.equals(player.number)) return false;
		if (position != null ? !position.equals(player.position) : player.position != null)
			return false;
		if (redCards != null ? !redCards.equals(player.redCards) : player.redCards != null)
			return false;
		if (sevenOfWeek != null ? !sevenOfWeek.equals(player.sevenOfWeek) : player.sevenOfWeek != null)
			return false;
		if (yellowCards != null ? !yellowCards.equals(player.yellowCards) : player.yellowCards != null)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = (int) (id ^ (id >>> 32));
		result = 31 * result + number.hashCode();
		result = 31 * result + name.hashCode();
		result = 31 * result + surname.hashCode();
		result = 31 * result + (position != null ? position.hashCode() : 0);
		result = 31 * result + (sevenOfWeek != null ? sevenOfWeek.hashCode() : 0);
		result = 31 * result + (MVP != null ? MVP.hashCode() : 0);
		result = 31 * result + (gamesPlayed != null ? gamesPlayed.hashCode() : 0);
		result = 31 * result + (goals != null ? goals.hashCode() : 0);
		result = 31 * result + (yellowCards != null ? yellowCards.hashCode() : 0);
		result = 31 * result + (redCards != null ? redCards.hashCode() : 0);
		result = 31 * result + (avatarURL != null ? avatarURL.hashCode() : 0);
		return result;
	}
}
